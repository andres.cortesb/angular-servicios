import { Component, OnInit } from '@angular/core';

/** Servicios */
import { GitSearchService } from '../git-search.service'

/** Modelos */
import { GitSearch } from '../git-search'

@Component({
  selector: 'ac-git-search',
  templateUrl: './git-search.component.html',
  styleUrls: ['./git-search.component.css']
})

export class GitSearchComponent implements OnInit {

  searchResults : GitSearch;
  searchQuery   : string;

  constructor(private _GitSearchService: GitSearchService) { }

  ngOnInit() {

    this._GitSearchService.gitSearch('angular').then( (response) => {
      this.searchResults = response;
    }, (error) => {
      alert("Error: " + error.statusText)
    })

  }

  gitSearch = () => {
    this._GitSearchService.gitSearch(this.searchQuery).then((response) => {
      this.searchResults = response;
    }, (error) => {
      alert("Error: " + error.statusText)
    })
  }

}
