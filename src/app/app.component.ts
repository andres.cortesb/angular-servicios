import { Component , OnInit } from '@angular/core';
import { GitSearchService } from './git-search.service';

@Component({
  selector: 'ac-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GitSearchService]
})

export class AppComponent implements OnInit{

  constructor(private GitSearchService: GitSearchService){
  }

  ngOnInit(){ }

  title = 'ac';

}
